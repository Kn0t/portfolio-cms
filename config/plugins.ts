export default ({ env }) => ({
  email: {
    config: {
      provider: 'sendgrid',
      providerOptions: {
        apiKey: env('SENDGRID_API_KEY'),
      },
      settings: {
        defaultFrom: 'nixname06@gmail.com',
        defaultReplyTo: 'erikmokross@gmail.com',
      },
    },
  },
})