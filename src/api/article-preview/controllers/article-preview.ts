/**
 * article-preview controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::article-preview.article-preview');
