/**
 * article-preview service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::article-preview.article-preview');
