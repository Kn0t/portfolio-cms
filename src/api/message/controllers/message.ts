/**
 * message controller
 */

interface EmailData {
  name: string;
  email: string;
  message: string;
}

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::message.message', ({ strapi }) => ({
  async create(ctx) {
    const options: EmailData = ctx.request.body.data;
    const msg = `Name: ${options.name}\nE-Mail: ${options.email}\n\n${options.message}`;

    try {
      //Send email to the user
      await strapi.plugins['email'].services.email.send({
        to: 'erikmokross@gmail.com',
        from: 'nixname06@gmail.com',
        replyTo: 'erikmokross@gmail.com',
        subject: 'Contact',
        text: msg
      });
      ctx.send({ok: true})
    } catch(err) {
      strapi.log.error(err);
      return ctx.badRequest
    }

    const res = await super.create(ctx);
    ctx.request.body.data
  }
}));
